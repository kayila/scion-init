class Turn {
  constructor(id, value, custom) {
    this.id = id;
    this.pr = Number.parseInt(value, 10);
    this.custom = custom;
  }
  // set pr(value) { pr = value; }
  // get pr() {
  //   return pr;
  // }
  copy() {
    return new Turn(id, pr, custom);
  }
  toJSON() {
    return {
      id: this.id,
      pr: this.pr.toString(10),
      custom: this.custom,
    }
  }
}

function insert(order, line, tick) {
  var slots = [];
  for (var x=0; x<8; x++) {
    slots[x] = [];
  }

  for (var x=0; x<order.length; x++) {
    var slot = order[x];
    slots[slot.pr].push(slot);
  }

  slots[line.pr].push(line);
  var newOrder = [];
  for (var x=0; x<8; x++) {
    newOrder = newOrder.concat(slots[(x+tick) % 8]);
  }

  return newOrder;
}

function getTurnOrder() {
  var turnOrder;
  turnOrder = Campaign().get("turnorder");
  if (turnOrder == undefined || turnOrder == "") {
    turnOrder = [];
  } else {
    try {
      turnOrder = JSON.parse(turnOrder, (key, value) => {
        if (key == "" && !(value instanceof Array)) {
          throw new SyntaxError("JsonString is not an array");
        }
        if (key == "pr") {
          return Number.parseInt(value, 10);
        }
        return value;
      });
      return turnOrder;
    } catch (error) {
      if (error instanceof SyntaxError) {
        log("ERROR: Inproperly formatted json: " + error);
        return [];
      }
      throw error;
    }
  }
}

function setTurnOrder(inputTurnOrder) {
  if (!(Array.isArray(inputTurnOrder))) {
    throw new SyntaxError("turnOrder must be an array!");
  }
  var turnOrder;
  turnOrder = JSON.stringify(inputTurnOrder, (key, value) => {
    if (key == "pr" && typeof value == "number") {
      return value.toString(10);
    }
    return value;
  });
  Campaign().set("turnorder", turnOrder);
}

function init() {
  var turnOrder = getTurnOrder();

  if (turnOrder.length < 1) {
    // Nothing to do
    return;
  }
  turnOrder.sort( (a, b) => b.pr - a.pr );
  var reactionCount = turnOrder[0].pr;
  turnOrder = turnOrder.map( x => {
    x.pr = reactionCount - x.pr;
    return x;
  }).map( x => {
      if (x.pr > 6)
        x.pr = "6";
      return x;
    });
  setTurnOrder(turnOrder);
}

function step(time) {
  var turnOrder = getTurnOrder();

  var activeTurn = turnOrder.shift();
  var currentSlot = activeTurn.pr;
  activeTurn.pr = (currentSlot + time) % 8;
  // Insert back into turn order
  turnOrder = insert(turnOrder, activeTurn, currentSlot);
  setTurnOrder(turnOrder);
}

function msgIsCommand(msg, cmd) {
  return msg.content.toLowerCase().indexOf(cmd) == 0;
}

function processChatMessage(msg) {
  if (msg.type != "api") { return };
  log("I found an api message: '" + msg.content + "'");
  if (msgIsCommand(msg, "!initturnorder")) {
    init();
  }
  if (msgIsCommand(msg, "!advanceturn")) {
    parts = msg.content.split(' ');
    if (parts.length != 2) {
      log("Wrong number of parts");
      return;
    }
    var time = Number.parseInt(parts[1], 10);
    if (! Number.isInteger(time)) {
      sendChat("scionInit", "/w " + msg.playerid +
          parts[1] + " is not a valid integer time value.");
      return;
    }
    step(time);
  }
}

function processTurnOrderChange(curr, prev) {
  /*
  log("Heard a change in the turn order");
  log("curr: " + JSON.stringify(curr, null, 4));
  log("prev: " + JSON.stringify(prev, null, 4));
  */
  if (curr.get("turnorder").length != prev.length) {
    log("Length of the turn order array changed, not running.");
    return;
  }

  // Prompt for new turn number
  sendChat("Initiative Wheel",
  "[Click here to set turn speed](!addturnspeed ?{Action Speed|0})");
}

module.exports = {
  Turn: Turn,
  getTurnOrder: getTurnOrder,
  setTurnOrder: setTurnOrder,
  init: init,
  step: step,
  insert: insert,
  msgIsCommand: msgIsCommand,
  processChatMessage: processChatMessage,
  processTurnOrderChange: processTurnOrderChange,
};

try {
  on("chat:message", processChatMessage);
  on("change:campaign:turnorder", processTurnOrderChange);
} catch (error) {
  console.warn("Whelp, guess I'm not in roll20...");
}
