"use strict";

const uuid = require('uuid/v4');
const scionInit = require("./scionInit");
var revert;

const cpgn = {
  fields: {},
  set: jest.fn().mockImplementation( (f, val) => {
    cpgn.fields[f] = val;
  }),

  get: jest.fn().mockImplementation( f => {
    return cpgn.fields[f];
  }),
};



beforeAll( () => {
  global.Campaign = function() {
    return cpgn;
  };
  global.log = jest.fn().mockImplementation((msg) => {
    // console.log(msg)
  });

  global.sendChat = jest.fn().mockImplementation((who, msg) => {
    // console.log("[" + who + "] " + msg);
  });
});

beforeEach( () => {
  cpgn.fields = {
    turnorder: JSON.stringify([]),
  };
  cpgn.get.mockClear();
  cpgn.set.mockClear();
  global.log.mockClear();
  global.sendChat.mockClear();
});

test("New slot added", () => {
  var processTurnOrderChange = scionInit.processTurnOrderChange;

  var turnOrder = [ entry("Jun", 0) ];
  cpgn.fields["turnorder"] = JSON.stringify(turnOrder);

  processTurnOrderChange(cpgn, JSON.stringify([]));
  expect(cpgn.set).not.toBeCalled();
  expect(JSON.parse(cpgn.fields["turnorder"])).toEqual(turnOrder);
  expect(global.sendChat).not.toBeCalled();
});

test("Slot deleted", () => {
  var processTurnOrderChange = scionInit.processTurnOrderChange;

  var turnOrder = [ entry("Jun", 0) ];

  processTurnOrderChange(cpgn, turnOrder);
  expect(cpgn.set).not.toBeCalled();
  expect(JSON.parse(cpgn.fields["turnorder"])).toEqual([]);
  expect(global.sendChat).not.toBeCalled();
});

test("Shift in order", () => {
  var processTurnOrderChange = scionInit.processTurnOrderChange;

  var turnOrder = [ entry("Jun", 0), entry("Hayden", 1) ];
  var nextTurnOrder = turnOrder.slice().reverse();
  cpgn.fields["turnorder"] = JSON.stringify(nextTurnOrder);

  processTurnOrderChange(cpgn, JSON.stringify(turnOrder));
  expect(cpgn.set).not.toBeCalled();
  expect(JSON.parse(cpgn.fields["turnorder"])).toEqual(nextTurnOrder);
  expect(global.sendChat).toBeCalled();
});

test("Init", () => {
  var init = scionInit.init;
  var getTurnOrder = scionInit.getTurnOrder;

  var startTurnOrder = [
    entry("Jun", 42),
    entry("Mai", 40),
    entry("Pup", 9),
  ];
  var expectedTurnOrder = [
    copy(startTurnOrder[0]),
    copy(startTurnOrder[1]),
    copy(startTurnOrder[2]),
  ];
  // And then fix the order fields
  expectedTurnOrder[0].pr = 0;
  expectedTurnOrder[1].pr = 2;
  expectedTurnOrder[2].pr = 6;

  // Set the starting condition
  cpgn.fields["turnorder"] = JSON.stringify(startTurnOrder);

  init();
  expect(cpgn.set.mock.calls.length).toBe(1);
  expect(getTurnOrder()).toEqual(expectedTurnOrder);
});

test("Init reverse", () => {
  var init = scionInit.init;
  var getTurnOrder = scionInit.getTurnOrder;

  var startTurnOrder = [
    entry("Jun", 0),
    entry("Mai", 2),
    entry("Pup", 4),
  ];
  var expectedTurnOrder = [
    copy(startTurnOrder[2]),
    copy(startTurnOrder[1]),
    copy(startTurnOrder[0]),
  ];
  // And then fix the order fields
  expectedTurnOrder[0].pr = 0;
  expectedTurnOrder[1].pr = 2;
  expectedTurnOrder[2].pr = 4;

  // Set the starting condition
  cpgn.fields["turnorder"] = JSON.stringify(startTurnOrder);

  init();
  expect(cpgn.set.mock.calls.length).toBe(1);
  expect(getTurnOrder()).toEqual(expectedTurnOrder);
});

test("Insert", () => {
  var insert = scionInit.insert;

  var turnOrder = [
    entry("Mai", 2),
    entry("Pup", 4),
    entry("Jun", 0),
  ];
  var newbie1 = entry("Andy", 3);
  var newbie2 = entry("Kria", 4);
  var newbie3 = entry("Carl", 1);

  var expected = turnOrder.slice();
  expected.splice(0, 0, newbie3);
  expected.splice(3, 0, newbie2);
  expected.splice(2, 0, newbie1);

  turnOrder = insert(turnOrder, newbie1, 1);
  turnOrder = insert(turnOrder, newbie2, 1);
  turnOrder = insert(turnOrder, newbie3, 1);

  expect(turnOrder).toEqual(expected);
});

test("step", () => {
  var step = scionInit.step;
  var getTurnOrder = scionInit.getTurnOrder;

  var turnOrder = [
    entry("Kria", 1),
    entry("Jun", 2),
    entry("Mai", 3),
    entry("Pup", 4),
  ];
  cpgn.fields["turnorder"] = JSON.stringify(turnOrder);

  var expected = turnOrder.slice();
  var moving = copy(expected.shift());
  moving.pr = 3;
  expected.splice(2, 0, moving);

  step(2);
  expect(cpgn.set.mock.calls.length).toBe(1);
  expect(getTurnOrder()).toEqual(expected);
});

test("getTurnOrder", () => {
  var getTurnOrder = scionInit.getTurnOrder;

  var expected = [
    entry("Jun", 1),
    entry("Pup", 2),
    entry("Hayden", 3),
  ];

  var jsonstr = [];
  for (var x=0; x<expected.length; x++) {
    var turn = copy(expected[x]);
    turn.pr = turn.pr.toString(10);
    jsonstr.push(turn);
  }
  jsonstr = JSON.stringify(jsonstr);

  cpgn.fields["turnorder"] = jsonstr;

  expect(getTurnOrder()).toEqual(expected);
});

test("setTurnOrder", () => {
  var setTurnOrder = scionInit.setTurnOrder;

  var turnOrder = [
    entry("Jun", 1),
    entry("Pup", 2),
    entry("Hayden", 3),
  ];

  var jsonstr = [];
  for (var x=0; x<turnOrder.length; x++) {
    var turn = copy(turnOrder[x]);
    turn.pr = turn.pr.toString(10);
    jsonstr.push(turn);
  }
  jsonstr = JSON.stringify(jsonstr);

  setTurnOrder(turnOrder);
  expect(cpgn.set).toHaveBeenCalled();
  expect(cpgn.fields["turnorder"]).toEqual(jsonstr);
});

test("Set and Get turn order", () => {
  var setTurnOrder = scionInit.setTurnOrder;
  var getTurnOrder = scionInit.getTurnOrder;

  var turnOrder = [
    entry("Jun", 1),
    entry("Pup", 2),
    entry("Hayden", 3),
  ];

  setTurnOrder(turnOrder);
  expect(getTurnOrder()).toEqual(turnOrder);
});

test("Turn.toJSON", () => {
  var Turn = scionInit.Turn;
  var expected = JSON.stringify({
    id: "fred",
    pr: "5",
    custom: "Jun",
  });

  var turn = new Turn("fred", "5", "Jun");
  var actual = JSON.stringify(turn);

  expect(actual).toEqual(expected);
});


function entry(text, value) {
  return {
    id: uuid(),
    pr: Number(value),
    custom: text,
  };
}

function copy(e) {
  return {
    id: e.id,
    pr: e.pr,
    custom: e.custom,
  };
}


